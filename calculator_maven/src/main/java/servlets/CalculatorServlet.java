package servlets;

import source.Calculator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


/**
 * Created by Vasiliy on 04.02.14.
 */
//@WebServlet(name = "CalculatorServlet", urlPatterns = {"calculator_maven/calc"},value = "calculator_maven/calc")
@WebServlet("/jsp/calc")
public class CalculatorServlet extends HttpServlet {
    private Calculator calculator = new Calculator();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        out.println("qwerty");
        String button = request.getParameter("button");
        if ((button.charAt(0) >= '0') && (button.charAt(0) <= '9')) {
            calculator.digit(Integer.parseInt("" + button));
        } else {
            switch (button) {
                case "+":
                    calculator.setOperation("+");
                    break;
                case "-":
                    calculator.setOperation("-");
                    break;
                case "*":
                    calculator.setOperation("*");
                    break;
                case "/":
                    calculator.setOperation("/");
                    break;
                case "C":
                    calculator.setOperation("C");
                case "=":
                    calculator.evaluate();
                    break;
            }
        }
        request.setAttribute("result", calculator.getResult());
        RequestDispatcher view = request.getRequestDispatcher("/calculator_maven/jsp/index.jsp");
        view.forward(request, response);
    }
}


