<%--
  Created by IntelliJ IDEA.
  User: Vasiliy
  Date: 04.02.14
  Time: 11:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<html>
<head>
    <title></title>
</head>

<body>
<form method="POST" action="/calculator">
    <input type="text" name="result" readonly="readonly" value="<c:out value="${result}"/>">
    <table>
        <tr>
            <td><input type="submit" name="button" value="1"></td>
            <td><input type="submit" name="button" value="2"></td>
            <td><input type="submit" name="button" value="3"></td>
            <td><input type="submit" name="button" value="+"></td>
        </tr>
        <tr>
            <td><input type="submit" name="button" value="4"></td>
            <td><input type="submit" name="button" value="5"></td>
            <td><input type="submit" name="button" value="6"></td>
            <td><input type="submit" name="button" value="-"></td>
        </tr>
        <tr>
            <td><input type="submit" name="button" value="7"></td>
            <td><input type="submit" name="button" value="8"></td>
            <td><input type="submit" name="button" value="9"></td>
            <td><input type="submit" name="button" value="*"></td>
        </tr>
        <tr>
            <td><input type="submit" name="button" value="0"></td>
            <td><input type="submit" name="button" value="C"></td>
            <td><input type="submit" name="button" value="="></td>
            <td><input type="submit" name="button" value="/"></td>
        </tr>
    </table>
    <br>
    <a name="Survey" href="survey.jsp">Survey</a>
</form>
</body>
</html>
